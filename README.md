# Asterisk Configs

This is our configuration for the Asterisk PBX, which is our SIP server and handles the telephony connectivity. 

## Installation

We are installing to a server on Google Cloud Platform running Debian 9. 
First make sure your repositories are up to date.

```
apt-get update
apt-get upgrade
```

Then install the required packages to compile asterisk.

```
apt-get install build-essential
apt-get install git-core subversion libjansson-dev sqlite autoconf automake libxml2-dev libncurses5-dev libtool
```

Then we download the Asterisk 17 source, which is the latest version as of writing.

```
cd /usr/src/
wget http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-17-current.tar.gz
tar -zxvf asterisk-17-current.tar.gz
```

Once the source archive is ready, change into the correct directory with the following command:

```
cd /usr/src/asterisk-17.0.1/
```

Use the install_prereq script to install all of the missing packages:

```
./contrib/scripts/install_prereq install
```

Next we run the configure script and the compilation itself, with the familiar series of commands.

```
./configure
make
make install
```

After that, Asterisk is installed! We just need to copy the configuration files to `/etc/asterisk/` and restart.

```
systemctl restart asterisk
```